# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/triage/reaction'

RSpec.describe Triage::Reaction do
  let(:path) { '/a/path' }
  let(:noteable_path) { '/a/noteable/path' }
  let(:body) { 'a body' }
  let(:discussion_id) { 1 }
  let(:discussion_path) { "#{noteable_path}/discussions/#{discussion_id}" }

  describe '#add_comment' do
    it 'calls post_request with expected params' do
      expect(Triage::Reaction).to receive(:post_request).with("#{noteable_path}/notes", body, append_source_link: false)

      described_class.add_comment(body, noteable_path, append_source_link: false)
    end
  end

  describe '#add_discussion' do
    it 'calls post_request with expected params' do
      expect(Triage::Reaction).to receive(:post_request).with("#{noteable_path}/discussions", body, append_source_link: true)

      described_class.add_discussion(body, noteable_path, append_source_link: true)
    end
  end

  describe '#append_discussion' do
    it 'calls post_request with expected params' do
      expect(Triage::Reaction).to receive(:post_request).with("#{discussion_path}/notes", body, append_source_link: false)

      described_class.append_discussion(body, discussion_id, noteable_path, append_source_link: false)
    end
  end

  describe '#resolve_discussion' do
    it 'calls put_request with expected params' do
      expect(Triage::Reaction).to receive(:put_request).with(discussion_path, resolved: true)

      described_class.resolve_discussion(discussion_id, noteable_path)
    end
  end

  describe '#unresolve_discussion' do
    it 'calls put_request with expected params' do
      expect(Triage::Reaction).to receive(:put_request).with(discussion_path, resolved: false)

      described_class.unresolve_discussion(discussion_id, noteable_path)
    end
  end

  describe '.post_request' do
    it 'performs a POST request' do
      expect_api_request(verb: :post, path: path, request_body: { body: body }) do
        expect(described_class.post_request(path, body)).to eq("POST #{Triage::PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`")
      end
    end

    it 'performs a POST request and appends source link' do
      source_path = 'some_automation.rb'
      expected_body = add_automation_suffix(source_path, body)
      expect(described_class).to receive(:get_source_path).and_return(source_path)

      expect_api_request(verb: :post, path: path, request_body: { body: expected_body }) do
        expect(described_class.post_request(path, body, append_source_link: true)).to eq("POST #{Triage::PRODUCTION_API_ENDPOINT}#{path}, body: `#{expected_body}`")
      end
    end

    context 'when source path cannot be determined' do
      it 'performs a POST request with no suffix' do
        expect(described_class).to receive(:get_source_path).and_return(nil)

        expect_api_request(verb: :post, path: path, request_body: { body: body }) do
          expect(described_class.post_request(path, body, append_source_link: true)).to eq("POST #{Triage::PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`")
        end
      end
    end
  end

  describe '.put_request' do
    it 'performs a PUT request' do
      expect_api_request(verb: :put, path: path, request_body: body) do
        expect(described_class.put_request(path, body)).to eq("PUT #{Triage::PRODUCTION_API_ENDPOINT}#{path}, body: `#{body}`")
      end
    end
  end
end
