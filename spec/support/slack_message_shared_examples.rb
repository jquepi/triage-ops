# frozen_string_literal: true

RSpec.shared_context 'with slack posting context' do
  let(:messenger_stub) { double }

  shared_examples 'no slack message posting' do
    it 'does not call #post_message_to_slack' do
      subject.process

      expect(messenger_stub).not_to have_received(:ping)
    end
  end

  shared_examples 'slack message posting' do
    it 'posts a Slack message' do
      subject.process

      expect(messenger_stub).to have_received(:ping).exactly(1).times.with(message_body)
    end
  end
end

RSpec.shared_examples 'processor slack options' do |slack_channel|
  context 'when slack options' do
    let(:expected_options) do
      {
        channel: slack_channel,
        username: Triage::GITLAB_BOT,
        icon_emoji: described_class::SLACK_ICON
      }
    end

    it 'has correct slack options' do
      expect(subject.slack_options).to eq(expected_options)
    end

    context 'with default messenger' do
      it 'instantiates slack messenger with the correct options' do
        expect(Slack::Messenger).to receive(:new).with(
          ENV['SLACK_WEBHOOK_URL'],
          expected_options
        )

        described_class.new(event)
      end
    end
  end
end
