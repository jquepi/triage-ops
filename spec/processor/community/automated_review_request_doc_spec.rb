# frozen_string_literal: true

require 'spec_helper'

require_relative '../../../triage/processor/community/automated_review_request_doc'

RSpec.describe Triage::AutomatedReviewRequestDoc do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        iid: merge_request_iid,
        project_web_url: 'https://gitlab.example/group/project'
      }
    end
    let(:from_gitlab_org) { true }
    let(:resource_open) { true }
    let(:merge_request_iid) { 300 }
    let(:added_label_names) { [Triage::AutomatedReviewRequestGeneric::WORKFLOW_READY_FOR_REVIEW_LABEL] }
  end

  let(:approvers) { %w[tech_writer1 tech_writer2] }
  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "app/models/misplaced/doc/user.md",
          "new_path" => "doc/user.md"
        }
      ]
    }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
    allow_any_instance_of(Triage::DocumentationCodeOwner).to receive(:approvers).and_return(approvers)
  end

  include_examples 'registers listeners', ["merge_request.update"]

  describe '#applicable?' do
    it_behaves_like 'community contribution open resource #applicable?'

    context 'when MR has Technical Writing label' do
      let(:label_names) { ['Technical Writing'] }

      include_examples 'event is not applicable'
    end

    context 'when MR has tw::triaged label' do
      let(:label_names) { ['tw::triaged'] }

      include_examples 'event is not applicable'
    end

    context 'when MR has tw::doing label' do
      let(:label_names) { ['tw::doing'] }

      include_examples 'event is not applicable'
    end

    context 'when "workflow::ready for review" is not added' do
      let(:added_label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when merge request does not change docs' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              'old_path' => 'app/models/user.rb',
              'new_path' => 'app/models/user_renamed.rb'
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when docs review was already requested' do
      before do
        allow(subject.__send__(:unique_comment)).to receive(:no_previous_comment?).and_return(false)
      end

      include_examples 'event is not applicable'
    end
  end

  describe '#documentation' do
    it_behaves_like 'processor documentation is present'
  end

  describe '#process' do
    shared_examples 'process docs merge request' do
      let(:extra_message) { "" }
      let(:expected_approver_usernames) { approvers.map { |username| "`@#{username}`" }.join(' ') }
      let(:expected_reviewer_usernames) { approvers.map { |username| "@#{username}" }.join(' ') }

      it 'posts a comment mentioning technical writer and labeling the merge request' do
        body = add_automation_suffix('community/automated_review_request_doc.rb') do
          text = <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            Hi #{expected_approver_usernames}! Please review this ~"documentation" merge request.
            #{extra_message}
          MARKDOWN
          text += "/assign_reviewer #{expected_reviewer_usernames}" if expected_reviewer_usernames

          %(#{text}\n/label ~"documentation" ~"tw::triaged")
        end

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when event is applicable' do
      include_examples 'process docs merge request'

      describe 'for approver groups' do
        shared_examples 'process docs merge request for approver group' do
          include_examples 'process docs merge request' do
            let(:approvers) { [approver_group] }
            let(:expected_approver_usernames) { "@#{approver_group}" }
            let(:expected_reviewer_usernames) { nil }
          end
        end

        context 'when approvers include @gl-docsteam' do
          include_examples 'process docs merge request for approver group' do
            let(:approver_group) { 'gl-docsteam' }
            let(:extra_message) { "\nPlease also consider updating the `CODEOWNERS` file in the #{event.project_web_url} project.\n" }
          end
        end

        context 'when approvers include @gitlab-com/runner-docs-maintainers' do
          include_examples 'process docs merge request for approver group' do
            let(:approver_group) { 'gitlab-com/runner-docs-maintainers' }
          end
        end
      end
    end

    context 'when there are no matching approvers' do
      let(:approvers) { [] }

      it 'only labels the merge request' do
        body = <<~MARKDOWN.chomp
          /label ~"documentation" ~"tw::triaged"
        MARKDOWN

        expect_comment_request(event: event, body: body) do
          subject.process
        end
      end
    end

    context 'when merge request changes docs/' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              'old_path' => 'docs/index.md',
              'new_path' => 'docs/index.md'
            }
          ]
        }
      end

      include_examples 'process docs merge request'
    end
  end
end
