# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class DeprecatedLabel < Processor
    DEPRECATED_LABEL_REPLACEMENT = {
      'Manage [DEPRECATED]' => 'devops::manage',
      'Create [DEPRECATED]' => 'devops::create',
      'Plan [DEPRECATED]' => 'devops::plan',
    }.freeze

    react_to 'issue.*', 'merge_request.*'

    def applicable?
      event.from_gitlab_org? &&
        deprecated_labels_added?
    end

    def process
      post_replacement_message
    end

    def documentation
      <<~TEXT
        This processor replaces deprecated label, if one is added, with the updated label.
      TEXT
    end

    private

    def deprecated_labels_added?
      !!deprecated_labels_added.any?
    end

    def deprecated_labels_added
      (event.added_label_names & DEPRECATED_LABEL_REPLACEMENT.keys)
    end

    def new_labels
      deprecated_labels_added.map { |label| DEPRECATED_LABEL_REPLACEMENT[label] }
    end

    def label_message(labels)
      labels.map { |l| %Q(~"#{l}") }.join(' ')
    end

    def post_replacement_message
      comment = <<~MARKDOWN.chomp
        Hey @#{event.event_actor_username}, please use #{label_message(new_labels)} as #{label_message(deprecated_labels_added)} has been deprecated.
        /unlabel #{label_message(deprecated_labels_added)}
        /label #{label_message(new_labels)}
      MARKDOWN
      add_comment(comment, append_source_link: true)
    end
  end
end
