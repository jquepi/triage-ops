# frozen_string_literal: true

require_relative '../triage/processor'

module Triage
  class LabelJiHuContribution < Processor
    LABEL_NAME = 'JiHu contribution'

    react_to 'merge_request.open'

    def applicable?
      event.jihu_contributor?
    end

    def process
      add_comment(%Q{/label ~"#{LABEL_NAME}"}, append_source_link: false)
    end

    def documentation
      <<~TEXT
        This processor labels merge requests contributed by JiHu with `~JiHu contribution`.
      TEXT
    end
  end
end
